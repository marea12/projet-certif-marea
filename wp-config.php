<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '<r&8o6`|WfnG^(k|,IaXuyD>TASPDL[c:Q6Jmk%L2K4Wv@qv=$aeqo^e_`tc1c7J' );
define( 'SECURE_AUTH_KEY',  'VMr @do#?E!Y[nY9N$:lH9q(rmIK?@MIhYp1!<ba4G#QKtneY`#rZ*`xtq!i-Etb' );
define( 'LOGGED_IN_KEY',    'ctm+fq<D#X+vVEc`RGNQ8m=[$xR5A.E8{V(F:otK18bl`,6tZi}([r.jyg*D#{ax' );
define( 'NONCE_KEY',        ',;wtMC>4pF|k*!PcgNW8i|^f|{39q0/Zuw}z.A[7Is x1NfBN$42OU-&JGl!*4Y^' );
define( 'AUTH_SALT',        'Hf.G+xFg>nXP*ZI(X+yUlSMjQ2U:>3:ypF$aB9v0^[3G~j0YA_f$:j^bSnZ84`E^' );
define( 'SECURE_AUTH_SALT', 'heuES)!I0M;Roy+P8aU(53qUsgs;dql5eK8U,>[kUSs5u9oa#Fpo]XW#NPbwG}VD' );
define( 'LOGGED_IN_SALT',   '>.9Tsd.<M(pk~H1,]8z2t2U=9%lUg+vKvsWi9,97w~2|GPGpsxJhvB1RW[q]gvh]' );
define( 'NONCE_SALT',       '4Dqi~}p_K6K}5kl_#Q~U<I]cf`8j@;2SRvnl^;Ys Id)kfjE7|q0DkrmuE`W}]C;' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
