<?php
/*
Plugin Name: certif plugin
Plugin URI: http://localhost:4000/
Description: avec ce plugin une certif réussie
Version: 0.1
Author: Marea Diallo
Author URI: http://localhost:4000/
License: GPL2
*/

function certif_init_plugin(){
  
  global $wpdb;
  
  $wp_certif_db_version = '1.0';
  $wp_contact = $wpdb->prefix . "certif_p_customers"; 
  $charset_collate = $wpdb->get_charset_collate();

  $sql = "CREATE TABLE $wp_contact (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    nom VARCHAR(100),
    sujet VARCHAR(100),
    email VARCHAR(255),
    messag VARCHAR(255),

    PRIMARY KEY  (id) 
  ) $charset_collate;";

  require_once( ABSPATH . 'contact.php' );
  dbDelta( $sql );

  add_option( 'plugin_name_db_version', $wp_certif_db_version );
}

register_activation_hook( __FILE__, 'certif_init_plugin' ); 


//$wp_contact = $wpdb->prefix . "wp_contact_customers"; 

//$sql = "DROP TABLE IF EXISTS $wp_contact";

//dbDelta( $sql );