<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class KleoPosts extends \Elementor\Widget_Base {

	public function get_name() {
		return 'kleo-posts';
	}

	public function get_title() {
		return esc_html__( 'Posts (Kleo)', 'kleo' );
	}

	public function get_icon() {
		return 'eicon-posts-group';
	}

	public function get_categories() {
		return [ 'kleo-elements' ];
	}

	protected function _register_controls() {

		global $kleo_config;

		$this->start_controls_section(
			'section_groups_settings',
			[
				'label' => esc_html__( 'Settings', 'kleo' ),
			]
		);

		$this->add_control(
			'post_layout',
			[
				'label'   => esc_html__( 'Listing type', 'kleo' ),
				'type'    => \Elementor\Controls_Manager::SELECT,
				'default' => 'list',
				'options' => [
					'grid'     => esc_html__( 'Grid', 'kleo' ),
					'small'    => esc_html__( 'Small', 'kleo' ),
					'standard' => esc_html__( 'Standard', 'kleo' ),
				]
			]
		);

		$this->add_control(
			'columns',
			[
				'label'     => esc_html__( 'Items per row', 'kleo' ),
				'type'      => \Elementor\Controls_Manager::SELECT,
				'default'   => '4',
				'options'   => [
					2,
					3,
					4,
					5,
					6
				],
				'condition' => [
					'post_layout' => 'grid'
				],
			]
		);

		if ( isset( $kleo_config['blog_layouts'] ) ) {
			$this->add_control(
				'show_switcher',
				[
					'label'        => esc_html__( 'Show Layout Switcher', 'kleo' ),
					'type'         => \Elementor\Controls_Manager::SWITCHER,
					'label_on'     => esc_html__( 'Yes', 'kleo' ),
					'label_off'    => esc_html__( 'No', 'kleo' ),
					'return_value' => '1',
					'default'      => '0',
					'description'  => esc_html__( 'This allows the visitor to change posts layout.', 'kleo' ),
				]
			);

			$this->add_control(
				'switcher_layouts',
				[
					'label'     => esc_html__( 'Switcher Layouts', 'kleo' ),
					'type'      => \Elementor\Controls_Manager::SELECT2,
					'default'   => array_keys( $kleo_config['blog_layouts'] ),
					'options'   => $kleo_config['blog_layouts'],
					'multiple'  => true,
					'condition' => [
						'show_switcher' => '1'
					],
				]
			);
		}

		$this->add_control(
			'show_thumb',
			[
				'label'     => esc_html__( 'Show Thumbnail image', 'kleo' ),
				'type'      => \Elementor\Controls_Manager::SELECT,
				'default'   => 'yes',
				'options'   => [
					'yes'    => esc_html__( 'Yes', 'kleo' ),
					'just_1' => esc_html__( 'Just first post', 'kleo' ),
					'just_2' => esc_html__( 'Just first two posts', 'kleo' ),
					'just_3' => esc_html__( 'Just first three posts', 'kleo' ),
					'no'     => esc_html__( 'No', 'kleo' ),
				],
				'condition' => [
					'post_layout' => 'standard'
				],
			]
		);

		$this->add_control(
			'show_meta',
			[
				'label'        => esc_html__( 'Show Post Meta', 'kleo' ),
				'type'         => \Elementor\Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Yes', 'kleo' ),
				'label_off'    => esc_html__( 'No', 'kleo' ),
				'return_value' => '1',
				'default'      => '1',
			]
		);

		$this->add_control(
			'inline_meta',
			[
				'label'        => esc_html__( 'Inline Post Meta', 'kleo' ),
				'type'         => \Elementor\Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Yes', 'kleo' ),
				'label_off'    => esc_html__( 'No', 'kleo' ),
				'return_value' => '1',
				'default'      => '0',
				'description'  => esc_html__( 'Applies to Standard Layout only. Shows the post meta elements in one line if enabled', 'kleo' ),
				'condition'    => [
					'show_meta' => '1'
				],
			]
		);

		$this->add_control(
			'show_footer',
			[
				'label'        => esc_html__( 'Show Post Footer', 'kleo' ),
				'type'         => \Elementor\Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Yes', 'kleo' ),
				'label_off'    => esc_html__( 'No', 'kleo' ),
				'return_value' => '1',
				'default'      => '1',
				'description'  => esc_html__( 'Show read more button and post likes', 'kleo' ),
			]
		);

		$this->add_control(
			'load_more',
			[
				'label'        => esc_html__( 'Enable Load More', 'kleo' ),
				'type'         => \Elementor\Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Yes', 'kleo' ),
				'label_off'    => esc_html__( 'No', 'kleo' ),
				'return_value' => '1',
				'default'      => '0',
				'description'  => esc_html__( 'Enable Load more posts via AJAX', 'kleo' ),
			]
		);


		$this->end_controls_section();

		$this->start_controls_section(
			'section_groups_query',
			[
				'label' => esc_html__( 'Query', 'kleo' ),
			]
		);

		$this->add_control(
			'query_post_types',
			[
				'label'    => esc_html__( 'Post Types', 'kleo' ),
				'type'     => \Elementor\Controls_Manager::SELECT2,
				'options'  => kleo_post_types( [ 'extra' => [ 'post' => 'Posts', 'page' => 'Pages' ] ] ),
				'default'  => [ 'post' ],
				'multiple' => true,
			]
		);

		$categoryArray = [];
		$categories    = get_categories( 'orderby=name&hide_empty=0' );

		foreach ( $categories as $category ) {
			$categoryArray[ $category->term_id ] = $category->name;
		}

		$this->add_control(
			'query_categories',
			[
				'label'    => esc_html__( 'Categories', 'kleo' ),
				'type'     => \Elementor\Controls_Manager::SELECT2,
				'options'  => $categoryArray,
				'multiple' => true,
			]
		);

		$tagsArray = [];
		$tags      = get_tags( array(
			'hide_empty' => false
		) );

		foreach ( $tags as $tag ) {
			$tagsArray[ $tag->term_id ] = $tag->name;
		}

		$this->add_control(
			'query_tags',
			[
				'label'    => esc_html__( 'Tags', 'kleo' ),
				'type'     => \Elementor\Controls_Manager::SELECT2,
				'options'  => $tagsArray,
				'multiple' => true,
			]
		);

		$this->add_control(
			'query_per_page',
			[
				'label'   => esc_html__( 'Number of posts', 'kleo' ),
				'type'    => \Elementor\Controls_Manager::NUMBER,
				'default' => 12,
			]
		);

		$this->add_control(
			'query_offset',
			[
				'label'   => esc_html__( 'Offset', 'kleo' ),
				'type'    => \Elementor\Controls_Manager::NUMBER,
				'default' => 0,
			]
		);

		$this->add_control(
			'query_sort',
			[
				'label'   => esc_html__( 'Sort', 'kleo' ),
				'type'    => \Elementor\Controls_Manager::SELECT,
				'default' => 'newest',
				'options' => [
					'newest' => esc_html__( 'Newest', 'kleo' ),
					'oldest' => esc_html__( 'Oldest', 'kleo' ),
					'random' => esc_html__( 'Random', 'kleo' ),
				]
			]
		);

		$this->end_controls_section();

	}

	protected function render() {
		$settings = $this->get_settings();


	}

	protected function _content_template() {
	}

}
